using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Upload_Dimensions
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var services = new ServiceCollection();
            
            var configuration = LoadConfiguration();
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            //ApplicationConfiguration.Initialize();
            //Application.Run(new Main());
            ConfigureServices(services, configuration);

            using (ServiceProvider serviceProvider = services.BuildServiceProvider())
            {
                var main = serviceProvider.GetRequiredService<Main>();
                Application.Run(main);
            }
        }


        private static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            // Register CsvFileParser Service
            services.AddTransient<CsvFileParser>();

            // Register UpdateDb Service
            services.AddTransient<UpdateDb>();

            // Register Logger Service
            services.AddTransient<Logger>();

            // Register Main Form
            services.AddScoped<Main>();

            // Add configuration into Service 
            services.AddSingleton<IConfiguration>(configuration);
        }

        private static IConfiguration LoadConfiguration()
        {
            return new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional:true, reloadOnChange: true)
                .Build();
        }
    }
}