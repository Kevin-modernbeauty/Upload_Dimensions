﻿namespace Upload_Dimensions
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            uploadButton = new Button();
            uploadFileText = new TextBox();
            fileUploadConfirmationLabel = new Label();
            updateDbButton = new Button();
            progressBar = new ProgressBar();
            SuspendLayout();
            // 
            // uploadButton
            // 
            uploadButton.Location = new Point(456, 42);
            uploadButton.Name = "uploadButton";
            uploadButton.Size = new Size(112, 34);
            uploadButton.TabIndex = 0;
            uploadButton.Text = "Upload";
            uploadButton.UseVisualStyleBackColor = true;
            uploadButton.Click += uploadButton_Click;
            // 
            // uploadFileText
            // 
            uploadFileText.Location = new Point(253, 44);
            uploadFileText.Name = "uploadFileText";
            uploadFileText.Size = new Size(197, 31);
            uploadFileText.TabIndex = 1;
            uploadFileText.Text = "Upload CSV File";
            // 
            // fileUploadConfirmationLabel
            // 
            fileUploadConfirmationLabel.AutoSize = true;
            fileUploadConfirmationLabel.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            fileUploadConfirmationLabel.ForeColor = Color.Red;
            fileUploadConfirmationLabel.Location = new Point(260, 86);
            fileUploadConfirmationLabel.Name = "fileUploadConfirmationLabel";
            fileUploadConfirmationLabel.Size = new Size(0, 25);
            fileUploadConfirmationLabel.TabIndex = 3;
            // 
            // updateDbButton
            // 
            updateDbButton.BackColor = Color.Red;
            updateDbButton.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            updateDbButton.ForeColor = Color.White;
            updateDbButton.Location = new Point(362, 189);
            updateDbButton.Name = "updateDbButton";
            updateDbButton.Size = new Size(112, 34);
            updateDbButton.TabIndex = 4;
            updateDbButton.Text = "Update DB";
            updateDbButton.UseVisualStyleBackColor = false;
            updateDbButton.Click += updateDbButton_Click;
            // 
            // progressBar
            // 
            progressBar.BackColor = SystemColors.ButtonHighlight;
            progressBar.Location = new Point(238, 229);
            progressBar.Name = "progressBar";
            progressBar.Size = new Size(379, 34);
            progressBar.Step = 2;
            progressBar.TabIndex = 2;
            // 
            // Main
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(progressBar);
            Controls.Add(updateDbButton);
            Controls.Add(fileUploadConfirmationLabel);
            Controls.Add(uploadFileText);
            Controls.Add(uploadButton);
            Name = "Main";
            Text = "Upload Dimensions";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button uploadButton;
        private TextBox uploadFileText;
        private TextBox fileUploadConfirmationText;
        private Label fileUploadConfirmationLabel;
        private Button updateDbButton;
        private ProgressBar progressBar;
    }
}
