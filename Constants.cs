﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upload_Dimensions
{
    public static class Constants
    {

        public static readonly List<string> OptFieldsList = new List<string>
        {
            "WIDTH",
            "HEIGHT",
            "LENGTH",
            "WEIGHT",
            "CASEWIDTH",
            "CASEHEIGHT",
            "CASELENGTH",
            "CASEWEIGHT",
            "CASELOT"
        };

    }
}
