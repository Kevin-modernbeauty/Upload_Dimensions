using System.Data;

namespace Upload_Dimensions
{
    public partial class Main : Form
    {

        private readonly CsvFileParser _csvFileParser;
        private readonly UpdateDb _updateDb;
        private OpenFileDialog fileDialog = new OpenFileDialog();
        private Logger _logger;

        public Main(CsvFileParser csvFileParser, UpdateDb updateDb, Logger logger)
        {
            InitializeComponent();
            _csvFileParser = csvFileParser;
            _updateDb = updateDb;
            _logger = logger;
        }



        private void uploadButton_Click(object sender, EventArgs e)
        {
            fileDialog.Filter = "CSV Files|*.csv";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                //fileDialog.ShowDialog();
                //MessageBox.Show(fileDialog.SafeFileName);

                uploadFileText.Text = fileDialog.SafeFileName;

                fileUploadConfirmationLabel.Text = $"{fileDialog.SafeFileName} is uploaded!";
            }


        }

        private void updateDbButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(fileDialog.FileName))
            {
                MessageBox.Show("Please upload CSV file before update db", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                string FileNameWithoutExtensions = Path.GetFileNameWithoutExtension(fileDialog.FileName);

                _logger.CreateLoggerFileName(FileNameWithoutExtensions);

                _logger.LogMessage($"File: {FileNameWithoutExtensions}");
                //CsvFileParser csvFileParser = new CsvFileParser();
                DataTable data = _csvFileParser.ParseCsv(fileDialog.FileName, _logger);


                _updateDb.StartUpdateDb(data, _logger, progressBar);

                Console.WriteLine("Finish");
                progressBar.Value = 0;
                _logger.resetLogFilePath();

                



            }
        }
    }
}
