﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upload_Dimensions
{
    public class UpdateDb
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        private readonly string _barcodeSearchTable;
        private readonly string _itemTable;
        private readonly string _vendItemSearchTable;
        private ProgressBar _progressBar;
        private Logger _logger;
        private string _currentItemNumber;
        private DataTable _dataTable;
        private int count = 0;
        private int countForProgress = 0;


        //public 
        public UpdateDb(IConfiguration configuration, Logger logger)
        {
            //InitializeComponent();
            //_dataTable = dataTable;
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
            _barcodeSearchTable = _configuration.GetSection("TableNames")["BarcodeSearchTable"];
            _itemTable = _configuration.GetSection("TableNames")["ItemTable"];
            _vendItemSearchTable = _configuration.GetSection("TableNames")["VendItemSearchTable"];
            //_logger = logger;
            //_logger = new Logger(_configuration.GetSection("FilePaths")["Logger"], )
        }

        public void StartUpdateDb(DataTable dataTable, Logger logger, ProgressBar progressBar)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                _logger = logger;
                _dataTable = dataTable;
                _progressBar = progressBar;
                int barcodeIndex = _dataTable.Columns.IndexOf("barcode");
                int vendItemIndex = _dataTable.Columns.IndexOf("venditem");
                int totalRows = dataTable.Rows.Count; 
                //_logger.LogMessage($"File: {dataTable.}");
                foreach (DataRow item in dataTable.Rows)
                {

                    countForProgress++;
                    var barcode = item[barcodeIndex].ToString();
                    var vendItem = item[vendItemIndex].ToString();

                    //if (string.IsNullOrEmpty(barcode))
                    //{
                    //    _logger.LogMessage("Barcode is empty in CSV File Row ");
                    //    continue;
                    //}



                    //Console.WriteLine(item.ToString());
                    string itemNumber = GetItemNumberFromBarcode(barcode, conn);

                    if (string.IsNullOrEmpty(itemNumber) )
                    {
                        if (!string.IsNullOrEmpty(vendItem))
                        {
                            itemNumber = GetItemNumberFromVendItem(vendItem, conn);
                        }

                        if (string.IsNullOrEmpty(itemNumber)) {
                            _logger.LogMessage($"Error: Failed to find item number using barcode '{barcode}' and venditem '{vendItem}'");
                            continue;
                        
                        }
                            
                    }

                    // If itemNumber is exist in ICITEMO Table, update values.
                    var existingFields = GetExistingOptFields(itemNumber, conn);

                    UpdateOrInsertItem(item, itemNumber, existingFields, conn);


                    count++;

                    int currentProgress = (int)Math.Round((double)countForProgress / totalRows * 100);
                    if (currentProgress > 100)
                    {
                        currentProgress = 100;
                    }
                    _progressBar.Invoke(new Action(() => _progressBar.Value = currentProgress));


                }
                _logger.LogMessage("");

                _logger.LogMessage($"Completed: Total {count} item(s) updated or inserted.");

                

                MessageBox.Show($"{count} item(s) are updated");
            }
        }

        private string GetItemNumberFromBarcode(string barcode, SqlConnection conn)
        {
            var query = $"SELECT RTRIM(ITEMNO) AS ITEMNO FROM {_barcodeSearchTable} WHERE MANITEMNO LIKE '{barcode}'";

            if (string.IsNullOrEmpty(barcode))
            {
                return "";
            }

            try
            {
                using (var cmd = new SqlCommand(query, conn))
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return reader["ITEMNO"].ToString()!;
                    }

                }
            }
            catch (Exception ex)
            {
                //_logger.LogMessage($"Error: Failed to find item number with barcode {barcode}");
                Console.WriteLine(ex.Message);

            }

            //_logger.LogMessage($"Error: Failed to find item number with barcode {barcode}");

            return "";

        }

        private string GetItemNumberFromVendItem(string vendItem, SqlConnection conn)
        {
            var query = $"SELECT RTRIM(ITEMNO) AS ITEMNO FROM {_vendItemSearchTable} WHERE VENDITEM LIKE '{vendItem}'";

            try
            {
                using (var cmd = new SqlCommand(query, conn))
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return reader["ITEMNO"].ToString()!;
                    }

                }
            }
            catch (Exception ex)
            {
                //_logger.LogMessage($"Error: Failed to find item number with barcode {barcode}");
                Console.WriteLine(ex.Message);

            }

            //_logger.LogMessage($"Error: Failed to find item number with barcode {barcode}");

            return "";

        }


        private List<string> GetExistingOptFields(string itemNumber, SqlConnection conn)
        {
            var query = $"SELECT RTRIM(ITEMNO) as ITEMNO, RTRIM(OPTFIELD) as OPTFIELD, RTRIM(VALUE) as VALUE FROM {_itemTable} WHERE ITEMNO = '{itemNumber}'";

            var optFields = new List<string>();

            try
            {
                using (var cmd = new SqlCommand(query, conn))
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var optField = reader["OPTFIELD"].ToString();
                        var value = reader["VALUE"].ToString();

                        

                        if (!Constants.OptFieldsList.Contains(optField.TrimEnd()))
                        {
                            continue;
                        }


                        optFields.Add(optField);
                    }
                }
            } catch (Exception ex)
            {
                _logger.LogMessage($"Error: {ex.Message}");
            }
            return optFields;
        }



        //private List<string> GetExistingOptFields(string itemNumber, SqlConnection conn)
        //{
        //    var query = $"SELECT RTRIM(ITEMNO) as ITEMNO, RTRIM(OPTFIELD) as OPTFIELD, RTRIM(VALUE) as VALUE FROM {_itemTable} WHERE ITEMNO = '{itemNumber}'";

        //    var optFields = new List<string>();

        //    try
        //    {
        //        using (var cmd = new SqlCommand(query, conn))
        //        using (var reader = cmd.ExecuteReader())
        //        {
        //            while (reader.Read())
        //            {
        //                var optField = reader["OPTFIELD"].ToString();
        //                var value = reader["VALUE"].ToString();

        //                if (!Constants.OptFieldsList.Contains(optField))
        //                {
        //                    continue;
        //                }

        //                optFields.Add(optField);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogMessage($"Error: {ex.Message}");
        //    }
        //    return optFields;
        //}


        private void UpdateOrInsertItem(DataRow item, string itemNumber, List<string> existingFields, SqlConnection conn)
        {
            var fieldsToInsert = Constants.OptFieldsList.Except(existingFields).ToList();
            var fieldsToUpdate = Constants.OptFieldsList.Intersect(existingFields).ToList();

            if (fieldsToInsert.Any() )
            {
                InsertItem(item, itemNumber, fieldsToInsert, conn);
            }

            if(fieldsToUpdate.Any() )
            {
                UpdateItem(item, itemNumber, fieldsToUpdate, conn);
            }

        }

        private void InsertItem(DataRow item, string itemNumber, List<string> fieldsToInsert, SqlConnection conn)
        {
            foreach (var field in fieldsToInsert)
            {
                //Find index of field
                int fieldIndex = _dataTable.Columns.IndexOf(field);

                string insertQuery = $"INSERT INTO {_itemTable} VALUES (@itemNumber, @optField, {DateTime.UtcNow.ToString("yyyyMMdd")},{DateTime.UtcNow.ToString("HHmmssff")}, 'KEVIN', 'MBSDAT', @value, 1, 60, 0, 0, 0, 1)";

                using (var insertCommand = new SqlCommand(insertQuery, conn))
                {
                    try
                    {
                        var value = item[fieldIndex].ToString();

                        insertCommand.Parameters.AddWithValue("@itemNumber", itemNumber);
                        insertCommand.Parameters.AddWithValue("@optField", field);
                        insertCommand.Parameters.AddWithValue("@value", value);

                        insertCommand.ExecuteNonQuery();

                        _logger.LogMessage($"Success: Item {itemNumber} field '{field}' has been inserted to {value}");
                        
                    }
                    catch (Exception ex)
                    {
                        _logger.LogMessage($"Error: Insert has been failed for Item {itemNumber}, and Field {field}");

                    }
                }

            }

        }

        private void UpdateItem(DataRow item, string itemNumber, List<string> fieldsToUpdate, SqlConnection conn)
        {

            foreach (var field in fieldsToUpdate)
            {
                //var insertQuery = $"INSERT INTO {Constants.ItemTable} (ITEMNO, OPTFIELD, VALUE) VALUES (@itemNumber, @optField, @value)";

                int fieldIndex = _dataTable.Columns.IndexOf(field);

                var updateQuery = $"UPDATE {_itemTable} SET VALUE = @value WHERE ITEMNO = @itemNumber AND OPTFIELD = @optField AND VALUE != @value";

                using (var updateCommand = new SqlCommand(updateQuery, conn))
                {
                    try
                    {
                        var value = item[fieldIndex].ToString();
                        updateCommand.Parameters.AddWithValue("@itemNumber", itemNumber);
                        updateCommand.Parameters.AddWithValue("@optField", field);
                        updateCommand.Parameters.AddWithValue("@value", value);
                        var rowAffected = updateCommand.ExecuteNonQuery();
                        _logger.LogMessage($"Success: Item {itemNumber} field {field} has been updated to {value}");
                    }
                    catch (Exception e)
                    {
                        _logger.LogMessage($"Error: Update has been failed for Item {itemNumber}, and Field {field}");
                    }
                }
            }
        }
    }
}
