﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upload_Dimensions
{
    public class CsvFileParser
    {
        private Logger _logger;
        public DataTable ParseCsv(string filePath, Logger logger)
        {
            DataTable dataTable = new DataTable();
            _logger = logger;

            using (TextFieldParser csvReader = new TextFieldParser(filePath))
            {
                csvReader.TextFieldType = FieldType.Delimited;
                csvReader.SetDelimiters(",");

                bool firstRow = true;

                while (!csvReader.EndOfData)
                {
                    string[] fields = csvReader.ReadFields();

                    if (firstRow)
                    {
                        foreach (string field in fields)
                        {
                            dataTable.Columns.Add(field);
                        }
                        firstRow = false;
                    }
                    else
                    {
                        // Skip to next row, if any of value has 0 or invalid value
                        //for (int i = 2; i < fields.Length-1; i++)
                        //{
                        //    if (string.IsNullOrEmpty(fields[i]) || fields[i].Equals("0"))
                        //    {
                        //        break;
                        //        return;
                        //    }
                        //}

                        bool isValidField = IsValidField(fields);
                        // 데이터 행 추가
                        if (isValidField)
                        {

                            dataTable.Rows.Add(fields);
                        }
                    }
                }
            }

            return dataTable;

        }

        private bool IsValidField(string[] fields)
        {
            for (int i = 2; i < fields.Length-1; i++)
            {
                if (string.IsNullOrEmpty(fields[i]) || fields[i].Equals("0"))
                {
                    _logger.LogMessage($"Error: Invalid value for Barcode '{fields[1]}', VendItem '{fields[0]}' item");

                    return false;
                }
            }

            return true;

        }
    }
}
