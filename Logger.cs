﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upload_Dimensions
{
    public class Logger
    {
        private readonly IConfiguration _configuration;
        private string _logFilePath;
        private string _logFolderPath;

        public Logger(IConfiguration configuration)
        {
            _configuration = configuration;
            _logFolderPath = _configuration.GetSection("FilePaths")["Logger"];

            if (!Directory.Exists(_logFolderPath))
            {
                Directory.CreateDirectory(_logFolderPath);
            }
            //_logFilePath = _logFilePath + $"log-{DateTime.UtcNow.ToString("yyyy-MM-dd-hh-mm-ss")}.txt";

        }

        public void CreateLoggerFileName(string brandName)
        {
            _logFilePath = _logFolderPath + $"log-{DateTime.UtcNow.ToString("yyyy-MM-dd-hh-mm-ss")}-{brandName}.txt";
        }

        public void LogMessage(string message)
        {
            try
            {


                if (!File.Exists(_logFilePath))
                {
                    FileStream fs = File.Create(_logFilePath);
                    fs.Close();
                }

                using (StreamWriter sw = new StreamWriter(_logFilePath, true))
                {
                    string log = "";
                    //if (isAddingDateTime)
                    //{
                    //    //sw.WriteLine($"{DateTime.UtcNow}: {message}");
                    //    log += $"{DateTime.UtcNow}: {message}";
                    //}
                    //else
                    //{
                    log += message;
                    //}


                    Console.WriteLine(message);
                    sw.WriteLine(log);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error writing to log file: " + e.Message);
            }
        }

        public void resetLogFilePath()
        {
            _logFilePath = "";
        }


    }
}
